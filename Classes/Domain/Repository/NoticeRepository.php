<?php
namespace HIVE\HiveExtCookie\Domain\Repository;

/***
 *
 * This file is part of the "hive_ext_cookie" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * The repository for Notices
 */
class NoticeRepository extends \HIVE\HiveCfgRepository\Domain\Repository\AbstractRepository
{

    /**
     * initializeObject
     */
    public function initializeObject()
    {
        $sUserFuncModel = 'HIVE\\HiveExtCookie\\Domain\\Model\\Notice';
        $sUserFuncPlugin = 'tx_hiveextcookie';
        parent::overrideQuerySettings($sUserFuncModel, $sUserFuncPlugin);
    }

}
