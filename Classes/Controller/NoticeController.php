<?php
namespace HIVE\HiveExtCookie\Controller;

/***
 *
 * This file is part of the "hive_ext_cookie" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * NoticeController
 */
class NoticeController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * noticeRepository
     *
     * @var \HIVE\HiveExtCookie\Domain\Repository\NoticeRepository
     * @inject
     */
    protected $noticeRepository = null;

    /**
     * action render
     *
     * @return void
     */
    public function renderAction()
    {


        $aHtml = [];
        $sCookie = 'hive-ext-cookie--';
        /*
         * Get current page uid
         */

        $iPageUid = $GLOBALS['TSFE']->id;
        /*
         * Find all AnnoyingPopup objects
         *
         * @var \HIVE\HiveCptCntAnnoyingPopup\Domain\Model\AnnoyingPopup[] $oAnnoyingPopups
         */

        $oNotices = $this->annoyingPopupRepository->findAll();
        /*
         * Iterate $aAnnoyingPopups
         */

        if ($oNotices != null) {


            if (!isset($_COOKIE[$sCookie . $oNotices->getUid()])) {
                $aNotice = [
                    $oNotices,
                    $oNotices->getLifetime() > 0 ? gmdate("D, d-M-Y H:i:s e", time() + $oNotices->getLifetime()) : '0'
                ];
                $aHtml[] = $aNotice;
            }

        }
        $this->view->assign('aHtml', $aHtml);


    }
}
