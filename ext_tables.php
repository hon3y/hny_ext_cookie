<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveExtCookie',
            'Hiveextcookienoticerender',
            'hive_ext_cookie :: Notice :: render'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_ext_cookie', 'Configuration/TypoScript', 'hive_ext_cookie');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextcookie_domain_model_notice', 'EXT:hive_ext_cookie/Resources/Private/Language/locallang_csh_tx_hiveextcookie_domain_model_notice.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextcookie_domain_model_notice');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
            'hive_ext_cookie',
            'tx_hiveextcookie_domain_model_notice'
        );

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder