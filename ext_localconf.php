<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveExtCookie',
            'Hiveextcookienoticerender',
            [
                'Notice' => 'render'
            ],
            // non-cacheable actions
            [
                'Notice' => 'render'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hiveextcookienoticerender {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_ext_cookie') . 'Resources/Public/Icons/user_plugin_hiveextcookienoticerender.svg
                        title = LLL:EXT:hive_ext_cookie/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_cookie_domain_model_hiveextcookienoticerender
                        description = LLL:EXT:hive_ext_cookie/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_cookie_domain_model_hiveextcookienoticerender.description
                        tt_content_defValues {
                            CType = list
                            list_type = hiveextcookie_hiveextcookienoticerender
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
call_user_func(
    function($extKey, $globals)
    {

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.hiveextcookienoticerender >
                wizards.newContentElement.wizardItems {
                    hive {
                        header = Hive
                        after = common,special,menu,plugins,forms
                        elements.hiveextcookienoticerender {
                            iconIdentifier = hive_cpt_brand_32x32_svg
                            title = Cookie (render)
                            description = Global cookie notice for website 
                            tt_content_defValues {
                                CType = list
                                list_type = hiveextcookie_hiveextcookienoticerender
                            }
                        }
                        show := addToList(hiveextcookienoticerender)
                    }

                }
            }'
        );

        // Register for hook to show preview of tt_content element of CType="yourextensionkey_newcontentelement" in page module
        // $globals['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem'][$extKey] = \HIVE\HiveExtEvent\Hooks\PageLayoutView\EventListPreviewRenderer::class;

    }, $_EXTKEY, $GLOBALS
);