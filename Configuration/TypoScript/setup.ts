
plugin.tx_hiveextcookie_hiveextcookienoticerender {
    view {
        templateRootPaths.0 = EXT:hive_ext_cookie/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hiveextcookie_hiveextcookienoticerender.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_ext_cookie/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hiveextcookie_hiveextcookienoticerender.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_ext_cookie/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hiveextcookie_hiveextcookienoticerender.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hiveextcookie_hiveextcookienoticerender.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

# these classes are only used in auto-generated templates
plugin.tx_hiveextcookie._CSS_DEFAULT_STYLE (
    textarea.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    input.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    .tx-hive-ext-cookie table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-hive-ext-cookie table th {
        font-weight:bold;
    }

    .tx-hive-ext-cookie table td {
        vertical-align:top;
    }

    .typo3-messages .message-error {
        color:red;
    }

    .typo3-messages .message-ok {
        color:green;
    }
)

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
plugin {
    tx_hiveextcookie {
        model {
            HIVE\HiveExtCookie\Domain\Model\Notice {
                persistence {
                    storagePid = {$plugin.tx_hiveextcookie.model.HIVE\HiveExtCookie\Domain\Model\Notice.persistence.storagePid}
                }
            }
        }
    }
}

plugin.tx_hiveextcookie_hiveextcookienoticerender {
    settings {
        render {

        }
    }
}


##
## Lib
## Render annoying popup
##
lib.tx_hiveextcookie_hiveextcookienoticerender = COA
lib.tx_hiveextcookie_hiveextcookienoticerender {
    10 = USER
    10 {
        userFunc = TYPO3\CMS\Extbase\Core\Bootstrap->run
        extensionName = HiveExtCookie
        pluginName = Hiveextcookienoticerender
        vendorName = HIVE
        controller = Notice
        action = render
        settings =< plugin.tx_hiveextcookie_hiveextcookienoticerender.settings
        persistence =< plugin.tx_hiveextcookie_hiveextcookienoticerender.persistence
        view =< plugin.tx_hiveextcookie_hiveextcookienoticerender.view
    }
}

##
## Fallback for TYPO3 <= 8.6.0 without HIVE and Gulp!
##
#[userFunc = HIVE\HiveExtCookie\Condition\Match\CurrentTypo3VersionOperatorGivenVersion::match("8.6.0", "<=")]
#    page {
#        includeJSFooterlibs {
#            HiveExtCookie__js = /typo3conf/ext/hive_ext_cookie/Resources/Public/Assets/Js/includeJSFooterlibs/index.js
#            HiveExtCookie__js.async = 1
#        }
#    }
#[global]